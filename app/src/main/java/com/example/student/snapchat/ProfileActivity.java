package com.example.student.snapchat;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ProfileFragment profile = new ProfileFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, profile).commit();
    }
}


