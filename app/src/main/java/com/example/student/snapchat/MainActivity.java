package com.example.student.snapchat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "84763545-7BA0-BD10-FF96-108035794200";
    public static final String SECRET_KEY = "23152402-3A43-286C-FFB5-831A839ED300";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);

        if(Backendless.UserService.loggedInUser() == "") {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }else {
            LoginFragment login = new LoginFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, login).commit();
        }
    }
}
